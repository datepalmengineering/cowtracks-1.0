from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.http import HttpResponse
from django.shortcuts import loader, render
from django.template import RequestContext
from forms import UploadLogForm
from models import Log
from django.views.decorators.csrf import csrf_exempt


@csrf_exempt
def upload_log(request):
    if request.method == 'POST':

        form = UploadLogForm(request.POST, request.FILES)
        if form.is_valid():
            new_log = form.save()
            if not thelog.name:
                # grabs just the filename itself (eliminates path)
                thelog.name = thelog.logfile.name.split('/')[-1]
            if Log.objects.filter(name=new_log.name).count() > 1:
                # Duplicates found! Deleting old ones.
                Log.objects.filter(
                    name=new_log.name).exclude(
                    pk=new_log.pk).delete()
            thelog = Log.objects.get(pk=new_log.pk)

            thelog.save()  # saves the new file name
            return HttpResponse(thelog.logfile.url)
        else:
            return HttpResponse("ERROR: INVALID FORM ENTRY!")
    else:
        form = UploadLogForm()

    template = loader.get_template('uploader/log_upload_form.html')
    context = RequestContext(request, {
        'form': form,
    })
    return HttpResponse(template.render(context))


def log_list(request):
    myLogs = Log.objects.all()

    template = loader.get_template('uploader/index.html')
    context = RequestContext(request, {
        'mylogs': myLogs,
    })
    return HttpResponse(template.render(context))
