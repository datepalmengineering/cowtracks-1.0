from django.forms import ModelForm
from models import Log

# Create the form class.


class UploadLogForm(ModelForm):

    class Meta:
        model = Log
        fields = ['name', 'host', 'build_number', 'logfile']
