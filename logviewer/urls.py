from django.conf.urls import include, url, patterns
from views import host_view, build_view, log_view


urlpatterns = patterns('',
                       url(r'^(?P<host>.*).com/$',
                           host_view,
                           name='host_view'),
                       url(r'^(?P<host>.*)/(?P<build_number>[0-9]+)/$',
                           build_view,
                           name='build_view'),
                       url(r'^(?P<host>.*)/(?P<build_number>[0-9]+)/(?P<logname>.*).log$',
                           log_view,
                           name='log_view'),
                       )
