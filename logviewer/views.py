from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.http import HttpResponse
from django.shortcuts import loader, render
from django.template import RequestContext
from uploader.models import Log
import requests


def host_view(request, host):
    host = host + ".com"
    myLogs = Log.objects.filter(
        host=host).values(
        'build_number',
        'host',
        'created_at').order_by("-build_number")
    newFancyLogs = []
    last_build = -1
    for log in myLogs:
        if log['build_number'] != last_build:
            last_build = log['build_number']
            newFancyLogs.append(log)
    if myLogs.count() == 0:
        return HttpResponseRedirect("http://" + host)
    template = loader.get_template('logviewer/build_list.html')
    context = RequestContext(request, {
        'mylogs': newFancyLogs,
    })
    return HttpResponse(template.render(context))


def build_view(request, host, build_number):
    myLogs = Log.objects.filter(host=host).filter(build_number=build_number)
    if myLogs.count() == 0:
        return HttpResponseRedirect("http://" + host)
    template = loader.get_template('logviewer/build_view.html')
    context = RequestContext(request, {
        'mylogs': myLogs,
    })
    return HttpResponse(template.render(context))


def host_list(request):
    myLogs = Log.objects.order_by().values('host').distinct().order_by("host")

    myCowVersions = []

    for host in myLogs:
        try:
            value = str(requests.get("http://" +
                                     str(host['host']) +
                                     "/cowversion", timeout=1).text.strip())
            myCowVersions.append({"host": str(host['host']), "value": value})
        except requests.exceptions.RequestException as e:
            myCowVersions.append(
                {"host": str(host['host']), "value": "OFFLINE"})

    template = loader.get_template('logviewer/hosts.html')

    context = RequestContext(request, {
        'myhosts': myLogs,
        'myCowVersions': myCowVersions,
    })
    return HttpResponse(template.render(context))


def log_view(request, host, build_number, logname):
    logname = logname + ".log"
    mylogcontent = None

    mylog = Log.objects.filter(
        host=host).filter(
        build_number=build_number).filter(
            name=logname)

    if mylog.count() > 0:
        if mylog[0].logfile is not None:
            try:
                with open(mylog[0].logfile.path, "r") as myfile:
                    mylogcontent = myfile.read()
            except:
                mylogcontent = "ERROR: Attempted to read, but " + \
                    mylog[0].logfile.path + " was Not Found."
        else:
            mylogcontent = "ERROR: No file has been uploaded with this log entry"
    else:
        raise Http404

    template = loader.get_template('logviewer/log_view.html')
    context = RequestContext(request, {
        "mylogcontent": mylogcontent,
        "mylog": mylog,
    })
    return HttpResponse(template.render(context))
