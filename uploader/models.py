from django.db import models
from uploader.storage import OverwriteStorage
# Create your models here.


def content_filename_name(instance, filename):
    myaddress = '/'.join(['logs', instance.host,
                          unicode(instance.build_number), filename])
    # incrementor = 1

    # while os.path.exists(myaddress):
    #    myaddress = '/'.join(['logs', instance.host, unicode(instance.build_number), str(incrementor)+"_"+filename])

    return myaddress


class Log(models.Model):
    name = models.CharField(max_length=255, blank=True, default=None)
    host = models.CharField(max_length=255)
    build_number = models.IntegerField()
    logfile = models.FileField(
        upload_to=content_filename_name,
        storage=OverwriteStorage(),
        blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):              # __unicode__ on Python 2
        return str(self.host + "/" + str(self.build_number) + "/" + self.name)
